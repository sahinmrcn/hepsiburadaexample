//
//  DetailView.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 19.10.2021.
//

import UIKit

class DetailView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var labelKey: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    
    //MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        Bundle.main.loadNibNamed("DetailView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        
    }
    
    func setupDetail(isSeparator: Bool, key: String, value: String) {
        viewSeparator.isHidden = isSeparator
        labelKey.text = key
        labelValue.text = value
    }
}
