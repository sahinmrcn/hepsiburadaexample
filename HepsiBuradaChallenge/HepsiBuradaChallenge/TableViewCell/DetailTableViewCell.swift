//
//  DetailTableViewCell.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 19.10.2021.
//

import UIKit
import Kingfisher
class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var stackViewDetail: UIStackView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }

    func setup(item: SearchItem?) {
        guard let item = item else {
            return
        }
        labelTitle.text = item.collectionName ?? ""
        
        if let imageURL = URL(string: item.artworkUrl100 ?? "") {
            imageTitle.kf.setImage(with: imageURL, placeholder: UIImage(), options: [.transition(.fade(1))])
        }
        
        stackViewDetail.removeAllArrangedSubviews()
        
        stackViewDetail.addArrangedSubview(detailAdd(key: "Başlık:", value: item.wrapperType))
        stackViewDetail.addArrangedSubview(detailAdd(key: "Tür:", value: item.kind))
        stackViewDetail.addArrangedSubview(detailAdd(key: "Sanatçı Adı:", value: item.artistName))
        stackViewDetail.addArrangedSubview(detailAdd(key: "Çıkış Tarihi:", value: configDate(releaseDate: item.releaseDate)))
        stackViewDetail.addArrangedSubview(detailAdd(key: "Ücret:", value: "\(item.collectionPrice ?? .zero)"))
        stackViewDetail.addArrangedSubview(detailAdd(key: "Ülke:", value: item.country))
        stackViewDetail.addArrangedSubview(detailAdd(key: "Birincil Tür Adı:", value: item.primaryGenreName))
        
        stackViewDetail.layoutIfNeeded()
        contentView.layoutIfNeeded()
    }
    
    func detailAdd(isSeparator: Bool = true, key: String, value: String?) -> UIView {
        let detailView = DetailView()
        detailView.setupDetail(isSeparator: isSeparator, key: key, value: value ?? "-")
        
        return detailView
    }
    
    func configDate(releaseDate: String?) -> String? {
        let timeZone: TimeZone = TimeZone(abbreviation: "GMT-3") ?? TimeZone.current
        guard let releaseDate = releaseDate,
        let convertDate = DateHelper.stringToDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss'Z'", dateString: releaseDate, timeZone: timeZone) else {
            return nil
        }
        
        return DateHelper.dateToString(dateFormat: "dd MMMM yyyy", dateNow: convertDate)
    }
}
