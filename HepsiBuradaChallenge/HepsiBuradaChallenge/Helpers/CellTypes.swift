//
//  CellTypes.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 17.10.2021.
//

import Foundation

enum CellTypes {
    enum SearchType: Int {
        case movies
        case music
        case apps
        case books
        
        var value: String {
            switch self {
            case .movies:
                return "movie"
            case .music:
                return "music"
            case .apps:
                return "software"
            case .books:
                return "ebook"
            }
        }
    }
}
