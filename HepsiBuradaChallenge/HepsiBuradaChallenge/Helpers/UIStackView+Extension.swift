//
//  UIStackView+Extension.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 19.10.2021.
//

import UIKit

extension UIStackView {
    func removeAllArrangedSubviews() {
        for view in self.arrangedSubviews {
            self.removeArrangedSubview(view)
            view.isHidden = true
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}
