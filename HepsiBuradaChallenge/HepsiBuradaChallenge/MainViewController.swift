//
//  MainViewController.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 17.10.2021.
//

import UIKit
import Alamofire

class MainViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var viewType: [UIView]!
    @IBOutlet var labelType: [UILabel]!
    @IBOutlet var buttonType: [UIButton]!
    
    //MARK: - Variables
    var items: [SearchItem] = []
    var searchType: CellTypes.SearchType?
    var serviceRun = false
    var toolBar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        setupXib()
        configType()
        setupToolBar()
    }
    
    func setupXib() {
        collectionView.register(UINib(nibName: MainMenuCollectionViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: MainMenuCollectionViewCell.reuseIdentifier)
        collectionView.register(UINib(nibName: EmptyCollectionViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: EmptyCollectionViewCell.reuseIdentifier)
        collectionView.reloadData()
    }
    
    func configType() {
        guard let searchType = searchType else {
            return
        }

        for (index, item) in viewType.enumerated() {
            item.backgroundColor = index == searchType.rawValue ? UIColor.white : UIColor.opaqueSeparator
        }
    }
    
    //MARK: - Button Actions
    @IBAction func clickedType(_ sender: UIButton) {
        guard let index = buttonType.firstIndex(where: { $0 == sender }),
        let searchType = CellTypes.SearchType(rawValue: index) else {
            return
        }
        
        self.searchType = searchType
        configType()
        getService(searchText: searchBar.text ?? "")
    }
}

//MARK: - UICollectionView Data Source
extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return serviceRun || items.isEmpty ? 1 : items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if !serviceRun && items.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EmptyCollectionViewCell.reuseIdentifier, for: indexPath) as! EmptyCollectionViewCell
            cell.setup()
            
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainMenuCollectionViewCell.reuseIdentifier, for: indexPath) as! MainMenuCollectionViewCell
        let item: SearchItem? = serviceRun ? nil : items[indexPath.row]
        cell.setup(item: item, serviceRun: serviceRun)
        
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = !serviceRun && items.isEmpty ? UIScreen.main.bounds.width : UIScreen.main.bounds.width / 2
        return CGSize(width: width, height: 250)
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < items.count {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = mainStoryBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            detailVC.item = items[indexPath.row]
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension MainViewController {
    func setupToolBar() {
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
        toolBar.sizeToFit()
        
        let closeButton = UIBarButtonItem(title: "Kapat", style: .plain, target: self, action: #selector(clickedToCloseButtonOnPickerView))
        let searchButton = UIBarButtonItem(title: "Ara", style: .plain, target: self, action: #selector(clickedToNextButtonOnPickerView))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([closeButton, spaceButton, searchButton], animated: false)
        
        searchBar.inputAccessoryView = toolBar
    }
    
    @objc func clickedToCloseButtonOnPickerView() {
        view.endEditing(true)
    }
    
    @objc func clickedToNextButtonOnPickerView() {
        view.endEditing(true)
    }
}

//MARK: - SearchBar Delegate
extension MainViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        getService(searchText: searchBar.text ?? "")
        view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        getService(searchText: searchBar.text ?? "")
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        getService(searchText: searchBar.text ?? "")
        view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getService(searchText: searchText)
    }
}

extension MainViewController {
    func searchToService(items: [SearchItem]?) {
        serviceRun = false
        self.items = items ?? []
        collectionView.reloadData()
    }
    
    func getService(searchText: String) {
        debugPrint("searchText: \(searchText)")
        guard 2 < searchText.count else {
            searchToService(items: nil)
            return
        }
        
        serviceRun = true
        let root = "https://itunes.apple.com/search?"
        let search = "term=\(searchText)"
        let limit = "&limit=20"
        var type = ""
        
        if let searchType = searchType {
            type = "&media=\(searchType.value)"
        }
        let serviceUrl = root + search + type + limit
        
        debugPrint(serviceUrl)
        
        AF.request(serviceUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData {  [weak self] response in
            switch response.result {
            case .success(let data):
                let item = try! JSONDecoder().decode(SearchResponseItem.self, from: data)
                
                self?.searchToService(items: item.results)
                
            case .failure(let error):
                ErrorHelper.showAlert(for: error, retryFunction: { self?.getService(searchText: searchText) })
            }
        }
    }
}
