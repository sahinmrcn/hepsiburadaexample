//
//  MainMenuCollectionViewCell.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 17.10.2021.
//

import UIKit
import SkeletonView
import Kingfisher

class MainMenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup(item: SearchItem?, serviceRun: Bool) {
        if serviceRun {
            imageTitle.showSkeleton()
            labelTitle.showSkeleton()
            labelPrice.showSkeleton()
            labelDate.showSkeleton()
        } else if let item = item {
            imageTitle.hideSkeleton()
            labelTitle.hideSkeleton()
            labelPrice.hideSkeleton()
            labelDate.hideSkeleton()
            
            labelTitle.text = item.collectionName ?? ""
            labelPrice.text = "Ücret: \(item.collectionPrice ?? 0.0)"
            configDate(releaseDate: item.releaseDate)
            
            if let imageURL = URL(string: item.artworkUrl100 ?? "") {
                imageTitle.kf.setImage(with: imageURL, placeholder: UIImage(), options: [.transition(.fade(1))])
            }
        }
    }
    
    func configDate(releaseDate: String?) {
        let timeZone: TimeZone = TimeZone(abbreviation: "GMT-3") ?? TimeZone.current
        guard let releaseDate = releaseDate,
        let convertDate = DateHelper.stringToDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ss'Z'", dateString: releaseDate, timeZone: timeZone) else {
            labelDate.text = ""
            return
        }
        
        let convertString = "Tarih: " + DateHelper.dateToString(dateFormat: "dd MMMM yyyy", dateNow: convertDate)
        labelDate.text = convertString
    }
}
