//
//  EmptyCollectionViewCell.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 17.10.2021.
//

import UIKit

class EmptyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    func setup() {
        labelTitle.text = "Aradğınız kriterlerde sonuç bulunamadı."
    }
}
