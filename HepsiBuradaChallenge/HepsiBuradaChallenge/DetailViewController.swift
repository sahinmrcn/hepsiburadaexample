//
//  DetailViewController.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 19.10.2021.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var tableViewDetail: UITableView!
    
    var item: SearchItem? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        setupXib()
    }
    
    func setupXib() {
        tableViewDetail.alwaysBounceVertical = false
        tableViewDetail.register(UINib(nibName: DetailTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: DetailTableViewCell.reuseIdentifier)
        
        tableViewDetail.reloadData()
    }
}

//MARK: - UITableView Data Source
extension DetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailTableViewCell.reuseIdentifier, for: indexPath) as! DetailTableViewCell
        cell.setup(item: item)
        
        return cell
    }
}

//MARK: - UITableView Delegate
extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
