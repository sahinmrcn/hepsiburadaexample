//
//  SearhcResponseItem.swift
//  HepsiBuradaChallenge
//
//  Created by sahin on 17.10.2021.
//

import UIKit

// MARK: - Welcome
struct SearchResponseItem: Codable {
    let resultCount: Int?
    let results: [SearchItem]?
}

struct SearchItem: Codable {
    let wrapperType: String?
    let kind: String?
    let artistName: String?
    let releaseDate: String?
    let collectionName: String?
    let artworkUrl100: String?
    let collectionPrice: Double?
    let country: String?
    let primaryGenreName: String?
}
